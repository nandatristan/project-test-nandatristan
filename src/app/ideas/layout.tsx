import axios from 'axios';
import Banner from '../components/Banner'

async function getData() {
    var token = "6b7508d89b5ab7216784d94a2c0147dac6e561bf6926a202d3c666daa7a994232d87c9c8a8c3e8dd5e309d989cfe909862fba8082880368598dd5ea3c9079032a82de07407e3ab45167be6aec83a9c683c95735383689dcd4562012f3468fef9a96b994b19a81ce7c0fc187d617f7440e0ec4892ce22af8354eae125bfc62aa8";

    const res = await axios.get(
        `https://brilliant-power-02cfef1556.strapiapp.com/api/banners/1?populate=*`,
        //using like this
        { headers: { Authorization: `Bearer ${token}` } }
    );
    const datafromAPI = res.data;
    // The return value is *not* serialized
    // You can return Date, Map, Set, etc.

    return datafromAPI
}

export default async function IdeasLayout({
    children, // will be a page or nested layout
}: {
    children: React.ReactNode
}) {
    const data = await getData()
    var baseUrl = "https://brilliant-power-02cfef1556.strapiapp.com"
    return (
        <section>
            {/* Include shared UI here e.g. a header or sidebar */}
            <div>
                <Banner title={data.data.attributes.title} imageUrl={data.data.attributes.image.data.attributes.url} subtitle={data.data.attributes.subtitle} />
                <div>
                </div>
            </div>
            <div className='m-20'>
                {children}
            </div>            
        </section>
    )
}