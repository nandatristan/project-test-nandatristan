"use client"
import axios from 'axios';
import { useEffect, useState } from 'react';
import { PaginationComponent } from "@/components/custom/Pagination";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import SortComponent from "../components/SortComponent"
import ItemCountComponent from "../components/ItemCountComponent"
import { format } from 'date-fns';
import { id } from 'date-fns/locale';


const getIdeasPage = async (page: number, sortby:string, pagecount:string) => {
    try {
        const res = await axios.get(
            `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${page}&page[size]=${pagecount}&append[]=medium_image&sort=${sortby}`,
            { headers: { "Accept": "application/json" } }
        );
        return res.data;
    } catch (error) {
        console.error('Error fetching ideas:', error);
        return null;
    }
};

const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    return format(date, 'dd MMMM yyyy', { locale: id });
  };
  

interface SearchParamsProps {
    searchParams?: {
        page?: string;
        pagecount?: string;
        sort?: string;
    };
}

const IdeasRoute = ({ searchParams }: SearchParamsProps) => {
    const [data, setData] = useState<any>(null);
    const currentPage = Number(searchParams?.page) || 1;
    const sortBy = searchParams?.sort || "-published_at";
    const pagecount = searchParams?.pagecount || "10";

    useEffect(() => {
        const fetchData = async () => {
            const data = await getIdeasPage(currentPage,sortBy,pagecount);
            setData(data);
        };

        fetchData();
    }, [currentPage]);

    if (!data) {
        return <div>Loading...</div>;
    }

    var pageCount = data.meta.last_page
    var total = data.meta.total
    var to = data.meta.to
    var from = data.meta.from

    return (
        <div>
            <div className='grid grid-cols-4 gap-4 p-4'>
                <div className='col-span-2'>
                    <h6 className='font-bold' >Showing {from} - {to} of {total}</h6>
                </div>
                <div>
                    <ItemCountComponent />
                    
                </div>
                <div>
                    <SortComponent />
                </div>
            </div>
            <br />
            <div className="grid grid-cols-1 gap-4 p-4">
                <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                    {data.data.map((item: any) => (
                        <Card className="relative" key={item.id}>
                            <img src="https://brilliant-power-02cfef1556.media.strapiapp.com/pexels_fauxels_3183150_1f4657bf60.jpg" alt="" />
                            <CardHeader>
                                <CardTitle className="text-gray-500 text-sm title-ellipsis">
                                    {formatDate(item.published_at)}
                                </CardTitle>
                                <CardTitle className="text-black-500 title-ellipsis">
                                    {item.title || "Idea Title"}
                                </CardTitle>
                            </CardHeader>
                        </Card>
                    ))}
                </div>
                <PaginationComponent pageCount={pageCount} />
            </div>
        </div>
    );
};

export default IdeasRoute;
