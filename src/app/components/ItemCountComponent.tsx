import Link from 'next/link';
import { useRouter, usePathname,  useSearchParams} from 'next/navigation'
import { useState, useCallback } from 'react';
export default function PagingClientComponent() {
   
    const pathname = usePathname();
    const searchParams = useSearchParams();
  
    const createPageURL = (pagecount: number | string) => {
      const params = new URLSearchParams(searchParams);
      params.set("pagecount", pagecount.toString());
      return `ideas?${params.toString()}`;
    };

    const pagecount = searchParams.get('pagecount')

    function onClick(pagecount: number | string) {
      window.location.href = createPageURL(pagecount)
    }
    
    return (
      
      <>
        <h5 className='font-bold' >Show per page</h5>
   
        {/* using useRouter */}
        <Link className={`${pagecount==='10' ? 'font-bold' : ''}`}
          href={createPageURL(10)}
          onClick={() => onClick(10)} 
        >
          10
        </Link>
   
        {/* using <Link> */}
        <Link className={`px-5 ${pagecount==='20' ? 'font-bold' : ''}`}
          href={createPageURL(20)}
          onClick={() => onClick(20)} 
        >
          20
        </Link>

        {/* using <Link> */}
        <Link className={`${pagecount==='50' ? 'font-bold' : ''}`}
          href={createPageURL(50)}
          onClick={() => onClick(50)} 
        >
          50
        </Link>
      </>
    )
  }