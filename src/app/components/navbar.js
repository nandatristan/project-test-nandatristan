"use client"

import Link from "next/link";
import React, { useEffect, useState } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import { usePathname } from 'next/navigation'

const Navbar = () => {
    const [nav, setNav] = useState(false);
    const pathname = usePathname().substring(1)
    const [show, setShow] = useState(true);
    const [lastScrollY, setLastScrollY] = useState(0);
    const [scrollingUp, setScrollingUp] = useState(false);

    const controlHeader = () => {
        if (typeof window !== 'undefined') {
            if (window.scrollY > lastScrollY) { // if scroll down
                setShow(false);
                setScrollingUp(false);
            } else { // if scroll up
                setShow(true);
                setScrollingUp(true);
            }
            setLastScrollY(window.scrollY);
        }
    };

    useEffect(() => {
        if (typeof window !== 'undefined') {
            window.addEventListener('scroll', controlHeader);

            return () => {
                window.removeEventListener('scroll', controlHeader);
            };
        }
    }, [lastScrollY]);

    const links = [
        {
            id: 1,
            link: "work",
        },
        {
            id: 2,
            link: "about",
        },
        {
            id: 3,
            link: "services",
        },
        {
            id: 4,
            link: "ideas",
        },
        {
            id: 5,
            link: "careers",
        },
        {
            id: 6,
            link: "contact",
        },
    ];

    return (
        <header className={`header ${show ? 'show' : ''} ${scrollingUp ? 'transparent' : ''}`}>
        <nav className="flex justify-between items-center w-full h-20 px-4 text-white bg-primary-suitmedia nav topnav">
            <div>
                {/* <h1 className="text-5xl font-signature ml-2"><a className="link-underline hover:transition ease-in-out delay-150 hover:underline hover:decoration-solid" href="">Logo</a></h1> */}
                <Link href="/" className="flex items-center space-x-3 rtl:space-x-reverse">
                    <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Suitmedia</span>
                </Link>
            </div>

            <ul className="hidden md:flex">
                {links.map(({ id, link }) => (
                    <li
                        key={id}
                        className={`nav-links px-4 cursor-pointer capitalize font-medium text-white hover:scale-105 duration-200 link-underline`}
                    >
                        <Link href={link} className={`link ${pathname === link ? 'active' : ''}`}>{link}</Link>
                    </li>
                ))}
            </ul>

            <div
                onClick={() => setNav(!nav)}
                className="cursor-pointer pr-4 text-white md:hidden"
            >
                {nav ? <FaTimes size={30} /> : <FaBars size={30} />}
            </div>

            {nav && (
                <ul className="flex flex-col justify-center items-center absolute top-0 left-0 w-full h-screen bg-gradient-to-b from-black to-gray-800 text-gray-500">
                    {links.map(({ id, link }) => (
                        <li
                            key={id}
                            className="px-4 cursor-pointer capitalize py-6 text-4xl"
                        >
                            <Link onClick={() => setNav(!nav)} href={link} className={`link ${pathname === link ? 'active' : ''}`}>
                                {link}
                            </Link>
                        </li>
                    ))}
                </ul>
            )}
        </nav>
        <style jsx>{`
        .header {
          position: fixed;
          top: 0;
          width: 100%;
          background-color: rgba(0, 0, 0, 0.8);
          color: white;
          transition: transform 0.3s ease-in-out, background-color 0.3s ease-in-out;
          transform: translateY(-100%);
          z-index: 9999;
        }
        .header.show {
          transform: translateY(0);
        }
        .header.transparent {
          background-color: rgba(0, 0, 0, 0.5);
        }
        nav ul {
          list-style: none;
          display: flex;
          justify-content: space-around;
          padding: 1em;
          margin: 0;
        }
        nav ul li a {
          color: white;
          text-decoration: none;
          font-size: 1.2em;
        }
        nav ul li.active a {
          border-bottom: 2px solid #fff;
        }
      `}</style>
        </header>
    );
};

export default Navbar;
