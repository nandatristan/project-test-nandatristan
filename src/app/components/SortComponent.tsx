import Link from 'next/link';
import { useRouter, usePathname,  useSearchParams} from 'next/navigation'
import { useState, useCallback } from 'react';
export default function SortClientComponent() {
   
    const pathname = usePathname();
    const searchParams = useSearchParams();
  
    const createPageURL = (sortBy: string) => {
      const params = new URLSearchParams(searchParams);
      params.set("sort", sortBy);
      return `ideas?${params.toString()}`;
    };

    const sort = searchParams.get('sort')

    function onClick(sort: string) {
      window.location.href = createPageURL(sort)
    }

    return (
      
      <>
        <h5 className='font-bold'>Sort By</h5>
   
        {/* using useRouter */}
        <Link className={`${sort==='-published_at' ? 'font-bold' : ''}`}
          href={createPageURL("-published_at")}
          onClick={() => onClick("-published_at")} 
        >
          Newest
        </Link>
   
        {/* using <Link> */}
        <Link className={`px-5 ${sort==='published_at' ? 'font-bold' : ''}`}
          href={createPageURL("published_at")}
          onClick={() => onClick("published_at")} 
        >
          Oldest
        </Link>
      </>
    )
  }