"use client"

import React from 'react';
import { Parallax } from 'react-parallax';

const Banner = ({ title, imageUrl, subtitle }) => {
  return (
    <Parallax bgImage={imageUrl} strength={300} className='polygon'>
        <div className="banner-content">
          <div className="grid grid-rows-2">
            <div><h1 className="text-5xl text-white uppercase items-center text-center title">{title}</h1></div>
            <div><h3 className="text-white text-center text-center">{subtitle}</h3></div>
          </div>
        </div>
      <style jsx>{`
        .banner-content {
          background: rgba(0, 0, 0, 0.5);
          color: white;
          padding-top: 10px;
          width: 100%;
          height: 100%;
          min-height: 70vh;
          top: 100%;
        }
        .title {
          margin-top: 20vh;
        }
      `}</style>
    </Parallax>
  );
};

export default Banner;
